from __future__ import unicode_literals
import os
from setuptools import setup, find_packages

NAME = 'fluent'
PACKAGES = find_packages()
DESCRIPTION = 'A Django translation system'
URL = "https://github.com/potatolondon/fluent"
LONG_DESCRIPTION = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()
AUTHOR = 'Potato London Ltd.'

setup(
    name=NAME,
    version='0.4.0',
    packages=PACKAGES,
    # metadata for upload to PyPI
    include_package_data=True,
    author=AUTHOR,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    keywords=["django", "translation", "Google App Engine", "GAE"],
    url=URL,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    install_requires=[
        'Django>=2.0,<4.0',
        'django-gcloud-connectors>=0.2.3',
        'google-cloud-datastore>=1.9.0',
        'sleuth-mock==0.1',
        'pyuca==1.2',
        'polib==1.1.0',
    ],
    extras_require={
        "tests": [
            'djangae @ git+https://github.com/potatolondon/djangae.git@86791fa843d09cf472a138135e74e32e8146aaf5',
        ]
    }
)
