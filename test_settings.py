import os
from django.urls import (
    include,
    path,
)

BASE_DIR = os.path.dirname(__file__)

INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'djangae',
    'djangae.tasks',
    'fluent',
)

DATABASES = {
    'default': {
        'ENGINE': 'gcloudc.db.backends.datastore',
        'INDEXES_FILE': os.path.join(os.path.abspath(os.path.dirname(__file__)), "djangaeidx.yaml"),
        "PROJECT": "test",
        "NAMESPACE": "",
    },
}

SECRET_KEY = "secret_key_for_testing"
USE_TZ = True

TEST_RUNNER = "xmlrunner.extra.djangotestrunner.XMLTestRunner"
TEST_OUTPUT_FILE_NAME = ".reports/django-tests.xml"
CLOUD_TASKS_LOCATION = "[LOCATION]"

# Point the URL conf at this file
ROOT_URLCONF = __name__

urlpatterns = [
    path('tasks/', include('djangae.tasks.urls')),
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
    },
]
